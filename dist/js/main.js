'use strict';

var burger = $('.header__burger');
var nav = $('.header__nav');
var burgerBtn = $('.header__burger-btn');

burger.on('click', function () {
        burgerBtn.toggleClass('active');
        nav.toggleClass('visible');
});